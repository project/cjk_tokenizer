<?php

/**
 * @cjk_tokenizer
 * Hooks provided by the cjk_tokenizer module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Add language support for east Asian countries except China, Japan and South Korea
 *
 * @see \Drupal\cjk_tokenizer\CJKTokenizerPluginManager::getPluginsBylangcode
 * @see cjk_tokenizer_form_search_admin_settings_alter(&$form, FormStateInterface $form_state, $form_id)
 *
 */
function hook_cjk_tokenizer_alter(&$plugins)
{
  //$plugins[$supportedlangcode][$plugin_id] = $pluginDefinition['label'];
}

/**
 * @} End of "addtogroup hooks".
 */
