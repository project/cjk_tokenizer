<?php

namespace Drupal\cjk_tokenizer\Plugin\CJKTokenizer;

use Drupal\cjk_tokenizer\ConfigurableCJKTokenizerBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Environment;

/**
 * Use baidu Chinese word segmentation API interface
 *
 * @CJKTokenizer(
 *   id = "baidu",
 *   label = @Translation("Baidu API tokenizer"),
 *   description = @Translation("Third-party API based on baidu search engine."),
 *   language_types = {
 *     "zh-hans",
 *     "zh-hant",
 *   },
 * )
 */
class BaiduTokenizer extends ConfigurableCJKTokenizerBase
{
  const BaiduURL = 'http://ai.baidu.com/tech/nlp/lexical';
  // Baidu API interface address, Go here to get the key information

  const TEXT_MAX_LENGTH = 20000;
  //The maximum number of bytes accepted by baidu API

  protected $errorLogs = []; //To prevent too many of the same error logs
  protected $logger = null;
  protected $baiduClient = null;

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode = NULL)
  {
    if (!$this->isNeedProcess($text, $langcode)) {
      return $text;
    }
    Environment::setTimeLimit(240); // Prevents script timeout interrupts, 240 is the default time for cron
    $sleep = ceil(1000000 / $this->configuration['qps']);

    $textArr = $this->splitText($text, $langcode);
    $processedText = '';
    foreach ($textArr as $str) {
      $processedText .= $this->doProcess($str, $langcode) . ' ';
      usleep($sleep);
    }
    return $processedText;
  }

  /**
   * do real processing
   *
   * @param string      $text
   *   Text to process.
   * @param string|null $langcode
   *   Language code for the language of $text, if known.
   *
   * @return string
   *   The processed text
   */
  protected function doProcess($text, $langcode = NULL)
  {
    $isFailed = false; //Indicates whether the baidu API call failed

    if (!$this->isNeedProcess($text, $langcode)) {
      return $text;
    }

    try {
      $client = $this->getBaiduClient();
      $result = $client->lexer($text);
      $sequence = ' ';
      if (isset($result['items']) && is_array($result['items'])) {
        foreach ($result['items'] as $item) {
          $sequence .= ($item['item'] ?: '') . ' ';
        }
        $text = $sequence;
      } else {
        $msg = 'API response exception';
        if (isset($result['error_msg'])) {
          $msg = $result['error_msg'];
        }
        throw new \Exception($msg);
      }
    } catch (\Exception $e) {
      $isFailed = true;
      $message = t('baidu tokenizer error:') . $e->getMessage();
      if (!in_array($message, $this->errorLogs)) {
        $this->errorLogs[] = $message;
        $this->getLogger()->warning($message);
      }
    }
    if ($isFailed) {
      $text = cjk_tokenizer_default_tokenizer($text, $langcode);
    }

    return $text;
  }

  /**
   * Process the text to the size required by baidu API
   *
   * @param string      $text
   * @param string|null $langcode
   *
   * @return array Split into an array of appropriately sized text
   */
  protected function splitText($text, $langcode = NULL)
  {
    $maxLimit = self::TEXT_MAX_LENGTH;
    $separator = ['，', '。', '！', '？', '；'];
    $textArr = [];
    if (strlen($text) <= $maxLimit) {
      $textArr[] = $text;
      return $textArr;
    }
    $strArr = preg_split("/(" . implode('|', $separator) . ")/", $text, NULL, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);
    $element = '';
    for ($i = 0; $i < count($strArr); $i++) {
      if (strlen($strArr[$i]) > $maxLimit) {
        continue; //An exception that should not occur
      }
      if (empty(trim($element)) && in_array($strArr[$i], $separator)) {
        continue; //Punctuation should not be at the beginning of a sentence
      }
      if (strlen($element . $strArr[$i]) > $maxLimit) {
        $textArr[] = $element;
        if (in_array($strArr[$i], $separator)) {
          $element = '';
        } else {
          $element = $strArr[$i];
        }
      } else {
        $element .= $strArr[$i];
      }
    }
    if (!empty(trim($element))) {
      $textArr[] = $element;
    }
    return $textArr;
  }

  /**
   * @param string      $text
   *   Text to process.
   * @param string|null $langcode
   *   Language code for the language of $text, if known.
   *
   * @return bool
   *   Whether the text needs to be processed, return true when need
   */
  protected function isNeedProcess($text, $langcode = NULL)
  {
    if (empty(trim($text, " \t\n\r\0\x0B" . chr(194) . chr(160)))) { //avoid send "&nbsp;"
      return false;
    } else {
      return true;
    }
  }

  /**
   * get Baidu client connection object
   *
   * @return \AipNlp
   */
  protected function getBaiduClient()
  {
    if (empty($this->baiduClient)) {
      $path = \Drupal::service("app.root") . '/' . \Drupal::moduleHandler()->getModule("cjk_tokenizer")->getPath();
      $path .= '/src/vendor/baidu/AipNlp.php';
      include_once $path;
      $this->baiduClient = new \AipNlp($this->configuration['appId'], $this->configuration['apiKey'], $this->configuration['secretKey']);
    }
    return $this->baiduClient;
  }

  /**
   * get Logger
   *
   * @return \Psr\Log\LoggerInterface|null
   */
  protected function getLogger()
  {
    if (empty($this->logger)) {
      $this->logger = cjk_tokenizer_getLogger();
    }
    return $this->logger;
  }


  /**
   * {@inheritdoc}
   */
  public function isApplicable($langcode = NULL)
  {
    // Network availability is checked in the configuration form, so returns directly here
    return true;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration()
  {
    return [
      'appId'     => '',
      'apiKey'    => '',
      'secretKey' => '',
      'qps'       => 2,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state)
  {
    //$form is passed with a null value

    $description = $this->getPluginDefinition()['description'] ?: '';
    $description .= t(' you need authorization information first. ');
    $description .= '<a href="' . self::BaiduURL . '" target="_blank">' . t('click here') . '</a><br>';
    $description .= t('recommend:') . t("Minimum word length to index") . ':2, ' . t("Number of items to index per cron run") . ':10';
    $form['description'] = [
      '#markup' => $description,
    ];
    $form['appId'] = [
      '#type'          => 'textfield',
      '#required'      => TRUE,
      //'#title'         => "appId",
      '#field_prefix'  => 'appId: ',
      '#default_value' => $this->configuration['appId'],
    ];
    $form['apiKey'] = [
      '#type'          => 'textfield',
      '#required'      => TRUE,
      '#field_prefix'  => "apiKey: ",
      '#default_value' => $this->configuration['apiKey'],
    ];
    $form['secretKey'] = [
      '#type'          => 'textfield',
      '#required'      => TRUE,
      '#field_prefix'  => "secretKey: ",
      '#default_value' => $this->configuration['secretKey'],
    ];
    $form['qps'] = [
      '#type'          => 'number',
      '#required'      => TRUE,
      '#min'           => 1,
      '#step'          => 1,
      '#field_prefix'  => "QPS limit: ",
      '#default_value' => $this->configuration['qps'],
      '#description'   => t('"query per second":If it is higher than the quota baidu gives you, API request may fail')
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state)
  {
    $appId = $form_state->getValue('appId');
    $apiKey = $form_state->getValue('apiKey');
    $secretKey = $form_state->getValue('secretKey');
    $path = \Drupal::service("app.root") . '/' . \Drupal::moduleHandler()->getModule("cjk_tokenizer")->getPath();
    $path .= '/src/vendor/baidu/AipNlp.php';
    include_once $path;

    // Make a real API request to verify that the parameters and network are available
    $text = '我是云客';
    try {
      $client = new \AipNlp($appId, $apiKey, $secretKey);
      $result = $client->lexer($text);
      $sequence = ' ';
      if (isset($result['items']) && is_array($result['items'])) {
        foreach ($result['items'] as $item) {
          $sequence .= ($item['item'] ?: '') . ' ';
        }
        $text = $sequence;
      } else {
        $msg = 'API response exception';
        if (isset($result['error_msg'])) {
          $msg = $result['error_msg'];
        }
        throw new \Exception($msg);
      }
    } catch (\Exception $e) {
      $message = t('baidu tokenizer error: ') . $e->getMessage() . ' , ';
      $message .= t('To check the parameters or network connections');
      $form_state->setError($form, $message);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state)
  {
    $this->configuration['appId'] = $form_state->getValue('appId');
    $this->configuration['apiKey'] = $form_state->getValue('apiKey');
    $this->configuration['secretKey'] = $form_state->getValue('secretKey');
    $this->configuration['qps'] = $form_state->getValue('qps');
  }

}
