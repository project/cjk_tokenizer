<?php

namespace Drupal\cjk_tokenizer\Plugin\CJKTokenizer;

use Drupal\cjk_tokenizer\ConfigurableCJKTokenizerBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * use the default word segmentation function of Drupal
 *
 * @CJKTokenizer(
 *   id = "default",
 *   label = @Translation("Drupal default tokenizer"),
 *   description = @Translation(" a simple Chinese/Japanese/Korean tokenizer based on overlapping sequences."),
 *   language_types = {
 *   },
 * )
 */
class OverlapTokenizer extends ConfigurableCJKTokenizerBase
{

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode = NULL)
  {
    return cjk_tokenizer_default_tokenizer($text, $langcode);
  }


  /**
   * {@inheritdoc}
   */
  public function isApplicable($langcode = NULL)
  {
    return true;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state)
  {
    $form['description'] = [
      '#markup' => $this->getPluginDefinition()['description'] ?: '',
    ];
    return $form;
  }

}
