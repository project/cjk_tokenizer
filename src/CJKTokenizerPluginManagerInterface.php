<?php


namespace Drupal\cjk_tokenizer;

use Drupal\Component\Plugin\PluginManagerInterface;


interface CJKTokenizerPluginManagerInterface extends PluginManagerInterface
{

  /**
   * According to the language code($langcode), the available plugins are returned.
   * If the language code is not passed, the available plugins for all language
   * codes will be returned.
   * If there is no available plugin, will return an empty array
   *
   * @param null $langcode
   *
   * @return array
   * The key name is the language code, the second-level key name is
   * the plugin ID, and the key value is the plugin label
   */
  public function getPluginsBylangcode($langcode = NULL);

}
