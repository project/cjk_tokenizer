<?php

namespace Drupal\cjk_tokenizer;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\cjk_tokenizer\CJKTokenizerPluginManagerInterface;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * cjk tokenizer plugin manager.
 *
 * @ingroup cjk_tokenizer
 */
class CJKTokenizerPluginManager extends DefaultPluginManager implements CJKTokenizerPluginManagerInterface
{
  /**
   * Stores the available plugin ids by language
   *
   * @var array
   */
  protected $plugins = [];

  /**
   * Constructs cjk tokenizer plugin manager.
   *
   * @param \Traversable           $namespaces
   * @param CacheBackendInterface  $cache_backend
   * @param ModuleHandlerInterface $module_handler
   *
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler)
  {
    parent::__construct('Plugin/CJKTokenizer', $namespaces, $module_handler, 'Drupal\cjk_tokenizer\CJKTokenizerInterface', 'Drupal\cjk_tokenizer\Annotation\CJKTokenizer');

    $this->setCacheBackend($cache_backend, 'CJKTokenizer');
    $this->alterInfo('CJKTokenizer');
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginsBylangcode($langcode = NULL)
  {
    if (empty($this->plugins)) {
      $definitions = $this->getDefinitions();
      $universalPlugins = [];
      foreach ($definitions as $plugin_id => $definition) {
        if (!empty($definition['language_types'])) {
          foreach ($definition['language_types'] as $supportedlangcode) {
            if (!isset($this->plugins[$supportedlangcode])) {
              $this->plugins[$supportedlangcode] = [];
            }
            $this->plugins[$supportedlangcode][$plugin_id] = $definition['label'];
          }
        } else {
          $universalPlugins[$plugin_id] = $definition['label'];
        }
      }
      foreach ($this->plugins as &$plugins) {
        $plugins = array_merge($universalPlugins, $plugins);
      }

      // Chinese, Japanese and Korean languages are provided by default
      if (empty($this->plugins['ja'])) {
        $this->plugins['ja'] = $universalPlugins;
      }
      if (empty($this->plugins['ko'])) {
        $this->plugins['ko'] = $universalPlugins;
      }
      //for other east Asian languages to use the default drupal tokenizer
      \Drupal::moduleHandler()->alter('cjk_tokenizer', $this->plugins);
    }
    if ($langcode == NULL) {
      return $this->plugins;
    } elseif (isset($this->plugins[$langcode])) {
      return $this->plugins[$langcode];
    } else {
      return [];
    }
  }

}
