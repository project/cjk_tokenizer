<?php


namespace Drupal\cjk_tokenizer;


interface CJKTokenizerInterface
{

  /**
   * Returns whether the tokenizer can run,
   * in some cases the tokenizer may not be able to run
   * For example: some tokenizers need specific PHP extension support,
   * but the extension is not installed
   * Another example: need internet support, but cannot access the internet
   * Returns Boolean TRUE when tokenizer is able to run,
   * otherwise returns a string stating why tokenizer is not able to run
   *
   * @param string|null $langcode
   *
   * @return true | string
   *   TRUE if the Tokenizer can be used, otherwise return string that describe the reason why it cannot be used
   */
  public function isApplicable($langcode = NULL);

  /**
   * Split text into tokens, returning a sequence separated by blank space
   * The transferred text has been HTML decoded, lowercased, and removed diacritics.
   * The implementation of this method needs to consider
   * the case where isApplicable returns non-true
   *
   * @param string      $text
   *   Text to process.
   * @param string|null $langcode
   *   Language code for the language of $text, if known.
   *
   * @return string|null
   *  The processed text
   *
   * @see search_simplify($text, $langcode = NULL)
   * @see \Drupal\Core\Language\LanguageManager::getStandardLanguageList()
   */
  public function process($text, $langcode = NULL);

}
