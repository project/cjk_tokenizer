<?php

namespace Drupal\cjk_tokenizer\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a CJK Tokenizer annotation object.
 *
 *
 * @Annotation
 *
 * @see \Drupal\cjk_tokenizer\CJKTokenizerPluginManager
 * @see \Drupal\cjk_tokenizer\CJKTokenizerInterface
 *
 * @ingroup cjk_tokenizer
 */
class CJKTokenizer extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the CJKTokenizer.
   *
   * @ingroup plugin_translatable
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $label;

  /**
   * A short description of the CJKTokenizer.
   *
   * @ingroup plugin_translatable
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $description;

  /**
   * The name of the CJKTokenizer class.
   *
   * This is not provided manually, it will be added by the discovery mechanism.
   *
   * @var string
   */
  public $class;

  /**
   * An array of language types the CJKTokenizer supports.
   * empty mean to applies to all languages
   *
   * @see \Drupal\Core\Language\LanguageManager::getStandardLanguageList()
   * @var array
   */
  public $language_types = [];


}
