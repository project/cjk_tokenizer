<?php

namespace Drupal\cjk_tokenizer;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\Extension\ModuleUninstallValidatorInterface;
use Drupal\cjk_tokenizer\CJKTokenizerPluginManagerInterface;
use Drupal\Core\Url;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Prevents uninstallation of modules providing used cjk tokenizer plugins.
 */
class CJKTokenizerUninstallValidator implements ModuleUninstallValidatorInterface
{

  use StringTranslationTrait;

  /**
   * cjk tokenizer plugin manager
   *
   * @var \Drupal\cjk_tokenizer\CJKTokenizerPluginManager
   */
  protected $CJKTokenizerManager;

  /**
   * modules providing cjk tokenizer plugin in use
   *
   * @var array
   */
  protected $modules = [];

  /**
   * language config
   *
   * @var array
   */
  protected $languages = [];

  /**
   * CJKTokenizerUninstallValidator constructor.
   *
   * @param \Drupal\cjk_tokenizer\CJKTokenizerPluginManagerInterface $CJKTokenizerManager
   * @param TranslationInterface                                     $string_translation
   * @param ConfigFactoryInterface                                   $config_factory
   */
  public function __construct(CJKTokenizerPluginManagerInterface $CJKTokenizerManager, TranslationInterface $string_translation, ConfigFactoryInterface $config_factory)
  {
    $this->CJKTokenizerManager = $CJKTokenizerManager;
    $this->stringTranslation = $string_translation;
    $this->languages = $config_factory->get('cjk_tokenizer.settings')->get('language');
  }

  /**
   * {@inheritdoc}
   */
  public function validate($module)
  {
    $reasons = [];
    if (empty($this->modules)) {
      foreach ($this->languages as $config) {
        if (empty($config['pluginID'])) {
          continue;
        }
        $provider = $this->CJKTokenizerManager->getDefinition($config['pluginID'])['provider'];
        if ($provider === 'cjk_tokenizer') {
          continue;
        }
        $this->modules[$provider] = true;
      }
    }
    if (array_key_exists($module, $this->modules)) {
      $reasons[] = $this->t('the module provides a CJK tokenizer in use, <a href=":url">Remove</a>', [':url' => Url::fromRoute('entity.search_page.collection')->toString(),]);
    }
    return $reasons;
  }
}
